import React from "react";
import ReactDOM from "react-dom/client";  // Змінено імпорт
import App from "./components/app";
import "./index.css";

const root = document.querySelector('.root');

const initializeApp = () => {
    const rootElement = document.createElement("div");
    root.appendChild(rootElement);

    ReactDOM.createRoot(rootElement).render(
        <React.StrictMode>
            <App />
        </React.StrictMode>
    );
}

initializeApp();
