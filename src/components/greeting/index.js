import style from "./greeting.module.css"
import mainImage from "../../img/main-image.png"

export default function Greeting() {

    return (
        <>
            <img className={style.mainImage} src={mainImage} alt=""/>
            <div className="container">

                <p className={style.work}>Frontend Developer</p>

                <div className={style.greeting}>
                    <p className={style.greeting__mainText}>Hello, my name is Bohdan Nemyrovskyi</p>
                    <p className={style.greeting__text}>I've been studying Front-End technologies for a year now, and this time was enough for me to make sure that this is my place in this industry.</p>
                </div>
                <div className={style.greeting__btns}>
                    <button className={style.btn__project}>Projects</button>
                    <button className={style.btn__linkedln}>LinkedIn</button>
                </div>
            </div>
        </>
    )

}