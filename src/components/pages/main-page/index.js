import NavMenu from "../../nav-menu"
import Greeting from "../../greeting"
import Projects from "../../projects"
import Form from "../../form"
import SocialMedia from "../../social-media"


export default function App() {

    return (
        <>
            <NavMenu></NavMenu>
            <Greeting></Greeting>
            <Projects></Projects>
            <Form></Form>
            <SocialMedia></SocialMedia>
        </>
    )

}