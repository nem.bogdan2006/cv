import NavMenu from "../../nav-menu";
import Form from "../../form";
import SocialMedia from "../../social-media";
import AboutMe from "../../about-me";

export default function About () {

    return (
        <>
        <NavMenu></NavMenu>
        <AboutMe></AboutMe>
        <Form></Form>
        <SocialMedia></SocialMedia>
        </>
    )

}