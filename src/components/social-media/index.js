import instagram from "../../img/icons/instagram.svg"
import linkedin from "../../img/icons/linkedin.svg"
import email from "../../img/icons/email.svg"
import style from "./social-media.module.css"
import finalImage from "../../img/final-image.svg"


export default function SocialMedia() {

    return (
        <>
            <div className="container">
                <div className={style.socialMedia}>
                    <div>
                        <img onClick={ () => window.location="https://www.instagram.com/bo.gdan3415/"} src={instagram} alt="" />
                        <img src={linkedin} alt="" />
                        <img src={email} alt="" />
                    </div>
                    <p>Bohdan Nemyrovskyi 2023</p>
                </div>
            </div>
            <img className={style.finalImage} src={finalImage} alt=""/>
        </>
    )

}