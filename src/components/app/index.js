import MainPage from "../pages/main-page"
import About from "../pages/about-me";
import Success from "../form/success";
import Fail from "../form/fail";
import { BrowserRouter, Route, Routes } from "react-router-dom";


export default function App() {

    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<MainPage/>}></Route>
                    <Route path="/about-me" element={<About/>}></Route>
                    <Route path="/success" element={<Success/>}></Route>
                    <Route path="/fail" element={<Fail/>}></Route>
                </Routes>
            </BrowserRouter>
            
        </>
    )

}