import { Link } from "react-router-dom"
import style from "./nav-menu.module.css"

export default function NavMenu() {

    return (
        <>
            <div className="container">
                <div className={style.navigation__menu}>
                    <p><Link to="/">Bohdan Nemyrovskyi</Link></p>
                    <ul className={style.nav__links}>
                        <li><Link to="/about-me"> About </Link></li>
                        <li>Projects</li>
                        <li>Contacts</li>
                    </ul>
                </div>
            </div>
        </>
    )

}