import projectImg1 from "../../img/project1.png"
import projectImg2 from "../../img/project2.png"
import projectImg3 from "../../img/project3.png"
import projectImg4 from "../../img/project4.png"
import style from "./projects.module.css"

export default function Projects() {
    return (
        <>
            <div className="container">
                <div className={style.cards}>
                    <div className={style.project__title}>
                        <p>Projects</p>
                        <span></span>
                    </div>

                    <div className={style.card}>
                        <div className={style.card__info}>
                            <p className={style.project__mainText}>Sportif</p>
                            <p className={style.card__text}>
                                "My first project is an online store that I developed by combining my skills as a web developer
                                and designer. This store was created to provide users with a convenient and safe shopping experience in the online environment.
                                It contains a wide range of products, a simple and intuitive interface for searching and ordering products.
                            </p>
                            <a href="/projects/sportif/" className={style.card__btn}>View Project</a>
                        </div>
                        <img className={style.cardImg} src={projectImg1} alt="" />
                    </div>

                    <div className={style.card}>
                        <img className={style.cardImg2} src={projectImg2} alt="" />
                        <div className={style.card__info}>
                            <p className={style.project__mainText}>Domino's Pizza</p>
                            <p className={style.card__text}>
                                "Domino's Pizza is an interactive web application designed to create and order customized pizzas online.
                                This project is designed to provide users with the opportunity to create their own unique pizzas by choosing ingredients,
                                size, sauces and other parameters.
                            </p>
                            <a href="/projects/pizza/" className={style.card__btn}>View Project</a>
                        </div>
                    </div>

                    <div className={style.card}>
                        <div className={style.card__info}>
                            <p className={style.project__mainText}>CRM</p>
                            <p className={style.card__text}>
                                A customer relationship management (CRM) system for internal use, designed to optimize the processes of adding and managing
                                products on a website. This project allows administrators to efficiently manage products and distribute them between different
                                Аdepartments of the website.
                            </p>
                            <a href="/projects/crm/" className={style.card__btn}>View Project</a>
                        </div>
                        <img className={style.cardImg} src={projectImg3} alt="" />
                    </div>


                    <div className={style.card}>
                        <img className={style.cardImg2} src={projectImg4} alt="" />
                        <div className={style.card__info}>
                            <p className={style.project__mainText}>Rick and Morty</p>
                            <p className={style.card__text}>
                                "Rick and Morty". This project was created to demonstrate my skills in developing web applications on the React platform and working
                                with API requests. It is based on the popular TV series "Rick and Morty" and provides users with the ability to get information
                                about the characters, locations, and episodes of this favorite animated series.
                            </p>
                            <a href="/projects/rick-and-morty/" className={style.card__btn}>View Project</a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )

}