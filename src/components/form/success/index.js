import style from "./success.module.css"
import { Link } from "react-router-dom"

export default function Success() {

    return (
        <>
            <div className={style.success}>
                <div>
                    <p>The message has been sent✅</p>
                    <p className={style.goBack}><Link to="/">Go back</Link></p>
                </div>
            </div>
        </>
    )

}