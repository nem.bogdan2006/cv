import React, { useState } from 'react';
import style from './form.module.css';

export default function Form() {
    const telegramBotToken = '6663190069:AAFPA6UueOFVJln2uOgz0rUgmYTh-JtJMWQ';

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    const [isButtonDisabled, setIsButtonDisabled] = useState(true);

    const sendTelegramMessage = async (message) => {
        const telegramAPIUrl = `https://api.telegram.org/bot${telegramBotToken}/sendMessage`;
        const chatId = '688462891';

        const formData = new URLSearchParams();
        formData.append('chat_id', chatId);
        formData.append('text', message);

        try {
            const response = await fetch(telegramAPIUrl, {
                method: 'POST',
                body: formData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
            });

            if (response.ok) {
                window.location = "/success"
            }
        } catch (error) {
            console.error('Помилка відправлення повідомлення у Telegram: ', error);
        }
    };

    const handleFormSubmit = (event) => {
        event.preventDefault();

        const telegramMessage = `Ім'я: ${name}\nEmail: ${email}\nПовідомлення: ${message}`;
        sendTelegramMessage(telegramMessage);
    };

    const handleInputChange = () => {
        if (name && email && message) {
            setIsButtonDisabled(false);
        } else {
            setIsButtonDisabled(true);
        }
    };

    return (
        <>
            <div className="container">
                <div className={style.project__title}>
                    <p>Projects</p>
                    <span></span>
                </div>
                <div className={style.form}>
                    <form onSubmit={handleFormSubmit}>
                        <div>
                            <div>
                                <p className={style.inputText}>Name</p>
                                <input
                                    className={style.name}
                                    type="text"
                                    value={name}
                                    onChange={(e) => {
                                        setName(e.target.value);
                                        handleInputChange();
                                    }}
                                    name="name"
                                />
                            </div>
                            <div>
                                <p className={style.inputText}>Email</p>
                                <input
                                    className={style.email}
                                    type="email"
                                    value={email}
                                    onChange={(e) => {
                                        setEmail(e.target.value);
                                        handleInputChange();
                                    }}
                                    name="email"
                                />
                            </div>
                            <div>
                                <p className={style.inputText}>Message</p>
                                <input
                                    className={style.message}
                                    type="text"
                                    value={message}
                                    onChange={(e) => {
                                        setMessage(e.target.value);
                                        handleInputChange();
                                    }}
                                    name="message"
                                />
                            </div>
                            <button
                                className={style.btn__submit} type="submit" disabled={isButtonDisabled}>Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
}
