const userInfoInputs = document.querySelectorAll('.grid input');

const user = {
    name: '',
    phoneNumber: '',
    email: ''
}

function checkInput() {
    const el = this;

    if (el.type === 'text' && validate(/^[а-яіїґє']+$/ig, el.value)) {
        el.classList.remove('error');
        el.classList.add('success');
        user.name = el.value;
        localStorage.user = JSON.stringify(user);
    } else if (el.type === 'tel' && validate(/^\+380\d{9}/, el.value)) {
        el.classList.remove('error');
        el.classList.add('success');
        user.phoneNumber = el.value;
        localStorage.user = JSON.stringify(user);
    } else if (el.type === 'email' && validate(/^[a-z0-9_.]+@[a-z]+[a-z.]*$/, el.value)) {
        el.classList.remove('error');
        el.classList.add('success');
        user.email = el.value;
        localStorage.user = JSON.stringify(user);
    } else {
        el.classList.remove('success');
        el.classList.add('error');
    }
}

if (localStorage.user) {
    userInfoInputs.forEach(el => {
        if (el.type === 'text' || el.type === 'tel' || el.type === 'email') {
            if (el.type === 'text') {
                el.value = JSON.parse(localStorage.user)['name'];
            } else if (el.type === 'tel') {
                el.value = JSON.parse(localStorage.user)['phoneNumber'];
            } else {
                el.value = JSON.parse(localStorage.user)['email']
            }
        }

    })


}


userInfoInputs.forEach(el => {
    if (el.type === 'text' || el.type === 'tel' || el.type === 'email') {
        el.addEventListener('input', checkInput);
    }
});

const btnSubmit = document.getElementById('btnSubmit');

btnSubmit.addEventListener('click', () => {
    let isValid = true;

    userInfoInputs.forEach(input => {
        if (input.type === 'text' || input.type === 'tel' || input.type === 'email') {
            checkInput.call(input);
            if (input.classList.contains('error')) {
                isValid = false;
            }
        }
    });

    if (isValid) {
        localStorage.user = JSON.stringify(user);
        window.location.href = "./thank you/index.html";
    } else {
        alert('Будь ласка, виправте помилки введення перед відправленням форми.');
    }
});




const validate = (r, t) => r.test(t);


// Ціна піци

const pizzaPrice = {
    size: [
        { size: 'small', price: 50 },
        { size: 'mid', price: 75 },
        { size: 'big', price: 100 },
    ],
    sauce: [
        { name: 'sauceClassic', price: 5, publicName: 'Кетчуп' },
        { name: 'sauceBBQ', price: 5, publicName: 'BBQ' },
        { name: 'sauceRikotta', price: 5, publicName: 'Рiкотта' },
    ],
    topping: [
        { name: 'moc1', price: 15, publicName: 'Сир звичайний' },
        { name: 'moc2', price: 10, publicName: 'Сир фета' },
        { name: 'moc3', price: 5, publicName: 'Моцарелла' },
        { name: 'telya', price: 20, publicName: 'Телятина' },
        { name: 'vetch1', price: 15, publicName: 'Помідори' },
        { name: 'vetch2', price: 10, publicName: 'Гриби' },
    ]
}



const pizza = {
    size: pizzaPrice.size[1],
    sauce: [],
    topping: []
}


function show(pizza) {
    let soucePrice = 0;
    let soucesName = [];

    pizza.sauce.forEach(sauce => {
        if (!soucesName.includes(sauce.publicName)) {
            soucePrice += sauce.price;
            soucesName.push(sauce.publicName);
        }
    });

    let toppingPrice = 0;
    let toppingsName = [];

    pizza.topping.forEach(topping => {
        if (!toppingsName.includes(topping.publicName)) {
            toppingPrice += topping.price;
            toppingsName.push(topping.publicName);
        }
    });

    document.getElementById('price').textContent = pizza.size.price + soucePrice + toppingPrice;
    document.getElementById('sauce').textContent = soucesName.join(', ');
    document.getElementById('topping').textContent = toppingsName.join(', ');
}




const [...radioInputs] = document.querySelectorAll('#pizza input');



radioInputs.forEach(radio => {
    show(pizza);
    radio.addEventListener('change', () => {
        pizza.size = pizzaPrice.size.find(el => {
            return el.size === radio.id;
        })
        show(pizza);
    })
})



// Перетягування добавків для піци

const tableScale = document.querySelector('.table-wrapper div img');

const draggableElements = document.querySelectorAll(".draggable");
const tableElement = document.querySelector(".table");

draggableElements.forEach(draggableElement => {
    draggableElement.addEventListener("dragstart", (e) => {
        e.dataTransfer.setData("text/plain", e.target.id);
        tableScale.style.scale = 1.05;
    });
});

tableElement.addEventListener("dragover", (e) => {
    e.preventDefault();
    tableScale.style.scale = 1;
});

tableElement.addEventListener("drop", (e) => {
    e.preventDefault();
    const id = e.dataTransfer.getData("text/plain");
    const draggableElement = document.getElementById(id);

    const imageCopy = draggableElement.cloneNode(true);
    tableElement.appendChild(imageCopy);

    if (id === 'sauceClassic' || id === 'sauceBBQ' || id === 'sauceRikotta') {
        const selectedSauce = pizzaPrice.sauce.find(el => el.name === id);
        if (selectedSauce) {
            pizza.sauce.push(selectedSauce);
        }
    } else {
        const selectedTopping = pizzaPrice.topping.find(el => el.name === id);
        if (selectedTopping) {
            pizza.topping.push(selectedTopping)
        }
    }
    tableScale.style.scale = 1;
    show(pizza)
});



// Знижка на піцу


const banner = document.querySelector('#banner');

banner.addEventListener('mouseover', () => {
    banner.addEventListener('mouseenter', () => {
        const offsetX = Math.floor(Math.random() * 80) + 1;
        const offsetY = Math.floor(Math.random() * 80) + 1;

        banner.style.right = offsetX + '%';
        banner.style.bottom = offsetY + '%';
    });
});

